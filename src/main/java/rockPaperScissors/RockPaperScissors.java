package rockPaperScissors;

import java.io.PrintStream;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

import javax.lang.model.util.ElementScanner14;
import javax.naming.spi.DirStateFactory.Result;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    public String isWinner(String HumCho, String ComCho) {
        String HumanWon = HumCho + " Human wins.";
        String ComWon = ComCho+ "Computer wins.";
        String Tie = "It's a tie";

        if (HumCho.equals("rock")){
            if (ComCho.equals("scissors")){
                humanScore +=1;
                return HumanWon;
            }
            else if (ComCho.equals("paper")){
                computerScore +=1;
                return ComWon;
            }
        }
        else if (HumCho.equals("paper")){
            if (ComCho.equals("rock")){
                humanScore +=1;
                return HumanWon;
            }
            else if (ComCho.equals("scissors")){
                computerScore +=1;
                return ComWon;
            }
        }
        else if (HumCho.equals("scissors")){
            if (ComCho.equals("paper")){
                humanScore +=1;
                return HumanWon;
            }
            else if (ComCho.equals("rock")){
                computerScore +=1;
                return ComWon;
            }
        }
        
        return Tie;
    }

    public String getRandomElement(List<String> rpsChoices){
        Random elemt = new Random();
        return rpsChoices.get(elemt.nextInt(rpsChoices.size()));
    }

    public void run() {
        // TODO: Implement Rock Paper Scissors
        boolean Continue = true;
        while(Continue) {
            //Handle computer random
            System.out.println("Let's play round " + roundCounter);
            roundCounter +=1;
            boolean part_of_game = true;
            while(part_of_game){
                String userInput = readInput("Your choice (Rock/Paper/Scissors)?");
                if (userInput.equals("rock")){
                    String UserChoice = userInput;
                    String ComputerChoice = getRandomElement(rpsChoices);
                    System.out.printf("Human chose %s, computer chose %s.",UserChoice, ComputerChoice);
                    String isWinning = isWinner(UserChoice, ComputerChoice);
                    System.out.println(isWinning);
                    System.out.printf("Score: human %d, computer %d\n", computerScore, humanScore);
                    part_of_game = false;
                }
                else if (userInput.equals("scissors")){
                    String UserChoice = userInput; 
                    String ComputerChoice = getRandomElement(rpsChoices);
                    System.out.printf("Human chose %s, computer chose %s.",UserChoice, ComputerChoice);
                    String isWinning = isWinner(UserChoice, ComputerChoice); 
                    System.out.println(isWinning);
                    System.out.printf("Score: human %d, computer %d\n", computerScore, humanScore);
                    part_of_game = false;
                }
                else if (userInput.equals("paper")){
                    String UserChoice = userInput;
                    String ComputerChoice = getRandomElement(rpsChoices);
                    System.out.printf("Human chose %s, computer chose %s.",UserChoice, ComputerChoice);
                    String isWinning = isWinner(UserChoice, ComputerChoice);
                    System.out.println(isWinning);
                    System.out.printf("Score: human %d, computer %d\n", computerScore, humanScore);
                    part_of_game = false;
                }
                else {
                    System.out.printf("I do not understand %s. Could you try again?", userInput);
                }
            };
            boolean IsWorng = true;
            while(IsWorng){
                String read_continue = readInput("Do you wish to continue playing? (y/n)?");
                if (read_continue.equals("y")){
                    IsWorng = false;
                    continue;
                }
                else if ( read_continue.equals("n")){
                    IsWorng = false;
                    Continue = false;
                    System.out.println("Bye bye :)");
                }
                else{
                    System.out.printf("I don't understand %s. Try again \n", read_continue);
                }
            }
        }

    }

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}
